require_relative './spec_helper.rb'
require 'securerandom'

describe 'sample spec', :type => :feature, :js => true do
  it 'works' do
    visit '/todos'
    expect(page).to have_content 'todo: Docker FTW'
  end


  it 'creates new todo' do
    visit '/todos'
    todo = SecureRandom.uuid

    input = page.find(:xpath, './/header/form/input')
    input.set(todo)
    input.native.send_keys :enter

    expect(page).to have_content todo
  end

  it 'marks todo as done' do
    visit '/todos'
    todo = SecureRandom.uuid

    input = page.find(:xpath, './/header/form/input')
    input.set(todo)
    input.native.send_keys :enter

    # Find the correct list item to mark done
    done_toggle = page.find('label', :text => todo).find(:xpath, './../form[1]/input[2]')
    done_toggle.set(true)

    expect(done_toggle.reload.checked?).to be_truthy
  end

  it 'deletes todo item' do
    visit '/todos'
    todo = SecureRandom.uuid

    input = page.find(:xpath, './/header/form/input')
    input.set(todo)
    input.native.send_keys :enter

    # Find the correct list item to mark done
    bttn = page.find('label', :text => todo).find(:xpath, './../form[2]/button', visible: false)
    #bttn = find(:xpath, '/html/body/section/section/ul/li[1]/div/form[2]/button', visible: false)

    bttn.trigger('click') # Need to trigger this way since the button is hidden unles mouse is over it

    expect(page).not_to have_content todo
  end




end